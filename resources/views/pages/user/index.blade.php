@extends('layouts.app')

@push('css')
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Users</h1>
                    </div><!-- /.col -->

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        @if (session('message'))
                            <div class="alert alert-success alert-dismissible fade show m-t-20 " role="alert">
                                {{ session('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h3 class="card-title">Available Users</h3>
                                    <a href="{{ route('users.create') }}"><button class="btn btn-primary">Create New User</button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="user-table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Roles</th>
                                            <th>Active</th>
                                            <th>API Token</th>
                                            <th>Created At</th>
                                            <th width="130px;">Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        var updateApiKey = function updateApiKey(context) {
            var userID = $(context).attr('data-id');
            var username = $(context).attr('data-username');

            swal({
                title: 'Are you sure want to generate new api key for "' + username + '"?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-sm btn-success',
                cancelButtonClass: 'btn btn-danger btn-sm m-l-10',
                confirmButtonText: 'Yes!'
            }).then(function (result) {
                if (!result.value) {
                    return;
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/users/' + userID + '/api_key',
                    type: 'PATCH',
                    success: function(result) {
                        if (result) {
                            swal({
                                title: "Success!",
                                text: "Successfully generate api key for " + username,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-success',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            }).then(function () {
                                $('.api-key-' + userID).html(result);
                            });
                        } else {
                            swal({
                                title: "Failed!",
                                text: "Failed to generate API key for " + username,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-danger',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            });
                        }
                    }
                });
            });
        };

        var deleteUser = function deleteUser(context) {
            var userID = $(context).attr('data-id');
            var username = $(context).attr('data-title');

            swal({
                title: 'Are you sure want to delete user "' + username + '"?',
                text: "You will not be able to recover this user after deleted!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (!result.value) {
                    return;
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/users/' + userID,
                    type: 'DELETE',
                    success: function(result) {
                        if (result) {
                            swal({
                                title: "Deleted!",
                                text: "User " + username + " has been deleted.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-success',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: "Deleting Failed!",
                                text: "Failed to delete user " + username,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-danger',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            }).then(function () {
                                location.reload();
                            });
                        }
                    }
                });
            })
        };

        $(function() {
            $('#user-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('users.get') !!}',
                columns: [
                    { data: 'id', name: 'id', searchable: false },
                    { data: 'username', name: 'username' },
                    { data: 'email', name: 'email' },
                    { data: 'roles', name: 'roles.name' },
                    { data: 'is_active', name: 'is_active' },
                    { data: 'api_token', name: 'api_token' },
                    { data: 'created_at', name: 'created_at', searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                drawCallback: function() {
                    $('.delete-btn').on('click', function () {
                        deleteUser(this);
                    });
                    $('.generate-api-key-btn').on('click', function () {
                        updateApiKey(this);
                    });
                }
            });
        });
    </script>
@endpush