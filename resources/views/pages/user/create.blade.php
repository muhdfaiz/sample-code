@extends('layouts.app')

@push('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet">
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Users</h1>
                    </div><!-- /.col -->

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h3 class="card-title">Create New User</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        @if (session('error'))
                                            <div class="alert alert-danger alert-dismissible fade show m-t-20 " role="alert">
                                                {{ session('error') }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif

                                        <form class="form-horizontal" action="{{ route('users.store') }}" method="post">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="username" class="col-sm-2 col-form-label">Username</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="text" id="username" name="username" value="{{ old('username') }}" required>
                                                    @error('username')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="email" id="email" name="email" value="{{ old('email') }}" required>
                                                    @error('email')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="text" id="phone" name="phone" value="{{ old('phone') }}">
                                                    @error('phone')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="api_token" class="col-sm-2 col-form-label">API Token</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="text" id="api_token" name="api_token" value="{{ old('api_token') }}" required>
                                                    <button type="button" class="mt-2 mb-2 btn btn-primary" id="generate-api-key">Generate</button>
                                                    @error('api_token')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password_confirmation" class="col-sm-2 col-form-label">Roles</label>
                                                <div class="col-sm-10">
                                                    <select class="form-control select2-roles" name="roles[]" multiple="multiple" data-placeholder="Select roles">
                                                        @foreach ($roles as $role)
                                                            <option value="{{ $role->name }}">{{ $role->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('roles')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="is_active" class="col-sm-2 col-form-label">Active</label>
                                                <div class="col-sm-10">
                                                    <input type="checkbox" data-toggle="toggle" data-size="sm" name="is_active">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-sm-2 col-form-label">Password</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="password" id="password" name="password" required>
                                                    <small id="password-help" class="form-text text-muted">Password must contain at least one digit, one lowercase character, one uppercase character and one special character.</small>
                                                    <span></span>
                                                    @error('password')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password_confirmation" class="col-sm-2 col-form-label">Password Confirmation</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control" type="password" id="password_confirmation" name="password_confirmation">
                                                    @error('password_confirmation')
                                                        <div class="error">
                                                            {{ $message }}
                                                        </div>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button type="submit" class="btn btn-primary">Create</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('js')
    <script src="{{ asset('plugins/select2/js/select2.full.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.select2-roles').select2();
        });
    </script>
    <script>
        function generateUUIDv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            )
        }

        $('#generate-api-key').click(function () {
            var apiToken = generateUUIDv4();

            $('#api_token').val(apiToken);
        })
    </script>
@endpush