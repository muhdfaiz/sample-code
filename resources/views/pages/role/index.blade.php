@extends('layouts.app')

@push('css')
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Roles</h1>
                    </div><!-- /.col -->

                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        @if (session('message'))
                            <div class="alert alert-success alert-dismissible fade show m-t-20 " role="alert">
                                {{ session('message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center justify-content-between">
                                    <h3 class="card-title">Available Roles</h3>
                                    <a href="{{ route('roles.create') }}"><button class="btn btn-primary">Create New Role</button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="role-table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Created At</th>
                                            <th width="130px;">Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('js')
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        var deleteRole = function deleteRole(context) {
            var roleID = $(context).attr('data-id');
            var roleTitle = $(context).attr('data-title');

            swal({
                title: 'Are you sure want to delete role "' + roleTitle + '"?',
                text: "You will not be able to recover this role after deleted!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger m-l-10',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (!result.value) {
                    return;
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '/roles/' + roleID,
                    type: 'DELETE',
                    success: function(result) {
                        if (result) {
                            swal({
                                title: "Deleted!",
                                text: "Role " + roleTitle + " has been deleted.",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-success',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                title: "Deleting Failed!",
                                text: "Failed to delete " + roleTitle,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: 'btn btn-danger',
                                confirmButtonText: "OK",
                                closeOnConfirm: false
                            }).then(function () {
                                location.reload();
                            });
                        }
                    }
                });
            })
        };

        $(function() {
            $('#role-table').DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                ajax: '{!! route('roles.get') !!}',
                columns: [
                    { data: 'id', name: 'id', searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'created_at', name: 'created_at', searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                drawCallback: function() {
                    $('.delete-btn').on('click', function () {
                        deleteRole(this);
                    });
                }
            });
        });
    </script>
@endpush