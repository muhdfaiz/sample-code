/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

!function($) {
    "use strict";

    var MainApp = function() {};

        MainApp.prototype.initActiveMenu = function () {
            // === following js will activate the menu in left side bar based on url ====
            $(".main-sidebar .nav-link").each(function () {
                var url = window.location.href.replace('/create', '').replace('#\/d\/edit#', '').replace('/invite', '')
                console.log(url, this.href)
                if (this.href.includes(url)) {
                    $(this).addClass("active");
                    $(this).parent().addClass("active"); // add active to li of the current link
                    $(this).parent().parent().addClass("in");
                    $(this).parent().parent().prev().addClass("active"); // add active class to an anchor
                    $(this).parent().parent().parent().addClass("active");
                    $(this).parent().parent().parent().parent().addClass("in"); // add active to li of the current link
                    $(this).parent().parent().parent().parent().parent().addClass("active");
                }
            });
        },

        MainApp.prototype.init = function () {
            this.initActiveMenu();
        },

        //init
        $.MainApp = new MainApp, $.MainApp.Constructor = MainApp
}(window.jQuery),

//initializing
    function ($) {
        "use strict";
        $.MainApp.init();
    }(window.jQuery);
