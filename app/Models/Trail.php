<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Trail extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table = 'trails';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*
    protected $fillable = [
        'eventversion',
        'useridentity',
        //'eventtime',
        'eventsource',
        'eventname',
        'awsregion',
        'sourceipaddress',
        'useragent',
        'errorcode',
        'errormessage',
        'resources',
        'requestparameters',
        'responseelements',
        'additionaleventdata',
        'requestid',
        'eventid',
        'eventtype',
        'apiversion',
        //'readonly',
        'recipientaccountid',
        'serviceeventdetails',
        'sharedeventid',
        'vpcendpointid',
    ];
    */

    protected $guarded = ['id'];

    /**
     * Set the eventtime attribute to datetime
     *
     * @param string value
     *
     * @return void
     */
    public function setEventtimeAttribute($value)
    {
        $this->attributes['eventtime'] = date('Y-m-d H:i:s', strtotime($value));
    }

    /**
     * Set the readonly attribute to a boolean
     *
     * @param string value
     *
     * @return void
     */
    public function setReadonlyAttribute($value)
    {
        // readonly column expects a boolean
        $this->attributes['readonly'] = (boolean)$value;
    }

    /**
     * Set the useridentity attribute to JSON
     *
     *  We receive some data with values that are in key-value pairs like so:
     * "{type=IAMUser, principalid=AIDAIIVLRNTITATNJKXTY, arn=arn:aws:iam::651110316028:user/OnPremTester,...}"
     *  So here we convert them to json
     *
     * @param  string $value
     *
     * @return void
     */
    public function setUseridentityAttribute($value)
    {
        $this->attributes['useridentity'] = $this->convert_kv_pairs_to_json($value);
    }

    /**
     * Set the resources attribute to JSON
     *
     *  We receive some data with values that are in key-value pairs like so:
     * "{type=IAMUser, principalid=AIDAIIVLRNTITATNJKXTY, arn=arn:aws:iam::651110316028:user/OnPremTester,...}"
     *  So here we convert them to json
     *
     * @param  string $value
     *
     * @return void
     */
    public function setResourcesAttribute($value)
    {
        $this->attributes['resources'] = $this->convert_kv_pairs_to_json($value);
    }

    /**
     * Converts a string containing key value pairs to valid JSON
     *
     * Example string: "{type=IAMUser, principalid=AIDAIIVLRNTITATNJKXTY,
     * arn=arn:aws:iam::651110316028:user/OnPremTester,...}"
     *
     * @param string $string The string to convert
     *
     * @return string a JSON formatted string
     */
    private function convert_kv_pairs_to_json($string)
    {
        $result = [];

        // my regex-fu isn't good enough for this,
        // so I do it the easy way, keeping in mind that
        // this might fail if the key or value has an "="

        $data = preg_replace('/{|}|[|]/', '', $string);

        $arr = explode(',', $data);
        foreach ($arr as $item) {
            $piece = explode('=', $item);
            $result[trim($piece[0])] = trim($piece[1]);
        }

        return json_encode($result);
    }
}
