<?php

/**
 * This middleware uses AWS KMS client to encrypt outgoing data responses and
 * decrypt incoming data requests (i.e. POST requests)
 */

namespace App\Http\Middleware;

use Aws\Kms\Exception\KmsException;
use Closure;
use AWS;

class EncryptionMiddleware
{
    private $KMSClient;
    private $plainDataKey; // used for encryption
    private $cipherDataKey; // attached with envelope for decryption
    private $context;
    private $cmkArn;

    protected $cacheLifetime = 5; // each key is used only for 5 minutes
    protected $cipherMethod = 'aes-128-gcm';


    public function __construct()
    {
        $this->KMSClient = AWS::createClient('KMS');

        $this->cmkArn = config('aws.kms.cmk_arn');
        $this->enabled = config('aws.kms.enable_encryption', true);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->enabled) {
            $this->initEncryption($request);

            // handle the response
            $response = $next($request);

            $collection = $response->original;
            $encryptedCollection = $this->_encryptEnvelope(json_encode($collection));
            $response->setContent(json_encode($encryptedCollection));

            // set a header so the requester knows there is encrypted data
            $response->header('Data-Encrypted', 'true');

            return $response;
        }

        return $next($request);
    }

    /**
     * Initialises the encryption by generating the encryption context
     * and getting the encryption data key
     *
     * @param $request
     *
     * @throws \Exception
     */
    protected function initEncryption($request)
    {
        $this->context = $this->_generateContext($request);

        $cacheKey = md5('CommsDataKey-' . implode($this->context));

        if (\Cache::has($cacheKey)) {
            $result = \Cache::get($cacheKey);
        } else {
            $result = $this->_generateDataKey($this->context);
            \Cache::put($cacheKey, $result, $this->cacheLifetime);
        }

        // extract the key
        $this->plainDataKey = $result->get('Plaintext');
        $this->cipherDataKey = $result->get('CiphertextBlob');
    }

    /**
     * Generates an associative array which is used as
     * the encryption context for the encryption process.
     *
     * @TODO determine what are the parameters which will be used to generate the context e.g. user id
     *
     * @param $request
     *
     * @return array
     */
    private function _generateContext($request)
    {
        $ctx = $request->header('ctx') ? $request->header('ctx') : null;
        $this->context = $this->setContext('ctx', $ctx);

        return $this->context;
    }

    /**
     * Requests a data key from AWS KMS
     *
     * @param array $context The encryption context, an associative array
     *
     * @return mixed
     * @throws \Exception
     */
    private function _generateDataKey(Array $context = [])
    {
        if (empty(implode($context))) {
            throw new \Exception('Missing encryption context.');
        }

        try {
            $result = $this->KMSClient->generateDataKey(
                [
                    'EncryptionContext' => $context,
                    // 'GrantTokens'       => ['<string>',],
                    'KeyId'             => $this->cmkArn, // REQUIRED
                    'KeySpec'           => 'AES_128',
                    // 'NumberOfBytes'     => 8,
                ]
            );
        } catch (\Aws\Kms\Exception\KmsException  $e) {
            throw new \Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * Encrypts the data into an encryption envelope containing the encrypted data and the
     * portable crypto materials required to decrypt it, using openssl
     *
     * @param $dataKey      The plaintext data key
     * @param $plainText    The plaintext data to encrypt
     *
     * @return array    The envelope containing the cipherText; data key; iv; and tag parameters used to encrypt
     */
    private function _encryptEnvelope($plainText)
    {
        $ivlen = openssl_cipher_iv_length($this->cipherMethod);
        $iv = openssl_random_pseudo_bytes($ivlen);

        $cipherText = openssl_encrypt($plainText, $this->cipherMethod, $this->plainDataKey, $options = 0, $iv, $tag);

        $envelope = [
            'cipherData' => base64_encode($iv . $cipherText . $tag),
            'cipherKey'  => base64_encode($this->cipherDataKey),
        ];

        return $envelope;
    }

    /**
     * Decrypt the encryption envelope using the provided crypto materials
     *
     * @param $cryptoParams      The crypto materials, comprising data key, iv and tags parameters
     * @param $cipherText        The encrypted text
     *
     * @return string       The decrypted string
     * @throws \Exception
     */
    private function _decryptEnvelope($envelope)
    {
        // parse the cipherData into its components
        $cipherData = base64_decode($envelope['cipherData']);
        $iv = substr($cipherData, 0, 12);
        $cipherText = substr($cipherData, 12, -16);
        $tag = substr($cipherData, -16);

        // get the key
        $dataKey = base64_decode($envelope['cipherKey']);
        $plainDataKey = $this->_decryptDataKey($dataKey);

        // decrypt the cipher text
        $decrypted = openssl_decrypt($cipherText, $this->cipherMethod, $plainDataKey, $options = 0, $iv, $tag);

        return $decrypted;
    }

    /**
     * Decrypt the data key using AWS KMS
     *
     * @param $context  The encryption context
     * @param $dataKey  The encrypted data key
     *
     * @return string   The decrypted string
     * @throws \Exception
     */
    private function _decryptDataKey($dataKey)
    {
        $context = $this->context;

        if (empty(implode($context))) {
            throw new \Exception('Missing encryption context.');
        }

        try {
            $result = $this->KMSClient->decrypt([
                'EncryptionContext' => $context,
                'CiphertextBlob'    => $dataKey,
            ]);
        } catch (\Aws\Kms\Exception\KmsException  $e) {
            throw new \Exception($e->getMessage());
        }

        return $result->get('Plaintext');
    }

    /**
     * Decrypt a given request
     *
     * @param $request
     * @param $context
     *
     * @return string
     * @throws \Exception
     */
    public function decryptEnvelope($envelope, $context)
    {
        $this->setContext($context);
        $decryptResult = $this->_decryptEnvelope($envelope);

        return $decryptResult;
    }


    /**
     * setter for context property, ensuring all keys are cast as string
     */
    protected function setContext($keyOrArray, $val = null)
    {
        if (!is_array($keyOrArray)) {
            $context[$keyOrArray] = (string)$val;
        } else {
            foreach ($keyOrArray as $key => $value) {
                $context[$key] = (string)$value;
            }
        }

        $this->context = $context;

        return $this->context;
    }
}
