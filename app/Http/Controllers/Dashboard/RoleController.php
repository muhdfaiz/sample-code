<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Role\CreateRequest;
use App\Http\Requests\Role\EditRequest;
use App\Services\RoleService;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;
use App\Models\Role;

class RoleController extends Controller
{
    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * RoleController constructor.
     *
     * @param RoleService $roleService
     */
    public function __construct(RoleService $roleService)
    {
      $this->roleService = $roleService;
    }

    /**
     * Display list of role available.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('pages.role.index');
    }

    /**
     * Get all role.
     *
     * @return mixed
     * @throws Exception
     */
    public function get()
    {
        return $this->roleService->getAllRole();
    }

    /**
     * Page to create new role.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('pages.role.create');
    }

    /**
     * Store new role in database.
     *
     * @param CreateRequest $request
     *
     * @return RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $result = $this->roleService->createNewRole($request->except('_token'));

        if ($result) {
            return redirect()->route('roles.index')->with('message', 'Successfully created role ' . $request->name . '.');
        }

        return redirect()->back()->with('error', 'Failed to create role ' . $request->name . '.');
    }

    /**
     * Display page to edit role.
     *
     * @param Role $role
     *
     * @return Factory|View
     */
    public function edit(Role $role)
    {
        return view('pages.role.edit', compact('role'));
    }

    /**
     * Update role information in database.
     *
     * @param Role $role
     * @param EditRequest $request
     *
     * @return void
     */
    public function update(Role $role, EditRequest $request)
    {
        $result = $this->roleService->updateRole($role, $request->except('_token'));

        if ($result) {
            return redirect()->back()->with('message',  'Role has been updated.');
        }

        return redirect()->back()->with('error', 'Failed to update ' . $role->name  . ' role');
    }

    /**
     * Delete role in database using role ID.
     *
     * @param Role $role
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Role $role)
    {
        $result = $this->roleService->deleteRole($role);

        return response()->json($result);
    }
}
