<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Display Dashboard Page.
     *
     * @return Factory|View
     */
    public function index()
    {
        $totalUser = User::select('id')->count();

        return view('pages.dashboard.index', compact('totalUser'));
    }
}
