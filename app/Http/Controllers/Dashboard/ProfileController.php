<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\EditRequest;
use App\Services\ProfileService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * @var ProfileService
     */
    private $profileService;

    /**
     * ProfileController constructor.
     *
     * @param ProfileService $profileService
     */
    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }


    /**
     * Display page to edit profile.
     *
     * @return Factory|View
     */
    public function edit()
    {
        $user = Auth::user();

        return view('pages.profile.edit', compact('user'));
    }

    /**
     * Update user profile.
     *
     * @param EditRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request)
    {
        $user = Auth::user();

        $result = $this->profileService->updateProfile($user, $request->except('token'));

        if ($result) {
            return redirect()->back()->with('message', 'Your profile has been updated.');
        }

        return redirect()->back()->with('error', 'Failed to update your profile.');
    }
}
