<?php

namespace App\Http\Requests\Profile;

use App\Rules\PasswordStrength;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userID = Auth::user()->id;

        return [
            'username' => 'required|unique:users,username,' . $userID,
            'email' => 'required|unique:users,email,' . $userID,
            'phone' => 'nullable|numeric',
            'password' => ['nullable', 'confirmed', 'min:14', new PasswordStrength],
        ];
    }
}
