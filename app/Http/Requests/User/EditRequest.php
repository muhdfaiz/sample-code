<?php

namespace App\Http\Requests\User;

use App\Rules\PasswordStrength;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users,username,' . $this->route('user')->id,
            'email' => 'required|unique:users,email,' . $this->route('user')->id,
            'phone' => 'nullable|numeric',
            'password' => ['nullable', 'confirmed', 'min:14', new PasswordStrength],
            'roles' => 'required'
        ];
    }
}
