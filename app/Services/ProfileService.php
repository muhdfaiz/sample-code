<?php

namespace App\Services;

use App\Models\User;

class ProfileService
{
    /**
     * Update user profile.
     *
     * @param User $user
     * @param array $inputs
     *
     * @return User|bool
     */
    public function updateProfile(User $user, array $inputs)
    {
        $password = empty($inputs['password']) ? $user->password : bcrypt($inputs['password']);

        $user->username = $inputs['username'];
        $user->email = $inputs['email'];
        $user->phone = $inputs['phone'];
        $user->password = $password;

        if ($user->save()) {
            return $user->refresh();
        }

        return false;
    }
}
