<?php

namespace App\Services;

use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleService
{
    /**
     * Retrieve all role using data table.
     *
     * @return mixed
     * @throws Exception
     */
    public function getAllRole()
    {
        return DataTables::of(Role::query())
            ->addColumn('action', function ($role) {
                if ($role->name === 'superadmin') {
                    return;
                }

                return '<a href="' . route("roles.edit", $role->id) . '"data-toggle="tooltip" data-placement="left" title="" data-original-title="Edit"' . ' class="btn btn-xs btn-info" style="width: 40px; padding: 5px; margin-bottom: 3px;"><i class="far fa-edit"></i></i></a>
                    <button type="button" class="delete-btn btn btn-xs btn-danger" style="width: 40px; padding: 5px; margin-bottom: 3px;"
                    data-id="' . $role->id . '" data-title="' . $role->name . '"><i class="far fa-trash-alt"></i></button>';
            })->rawColumns(['action'])->toJson();
    }

    /**
     * Store new role in database,
     *
     * @param array $inputs
     *
     * @return bool|Model|Role
     */
    public function createNewRole(array $inputs)
    {
        if ($result = Role::create($inputs)) {
            return $result;
        }

        return false;
    }

    /**
     * Update existing role in database,
     *
     * @param Role $role
     * @param array $inputs
     *
     * @return bool|Model|Role
     */
    public function updateRole(Role $role, array $inputs)
    {
        if ($result = $role->update($inputs)) {
            return $result;
        }

        return false;
    }

    /**
     * Delete role from database by role ID.
     *
     * @param Role $role
     *
     * @return int
     * @throws Exception
     */
    public function deleteRole(Role $role)
    {
        if ($role->delete()) {
            return true;
        }

        return false;
    }
}
