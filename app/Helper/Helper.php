<?php

use Illuminate\Support\Collection;

/**
 * Generate <li> elements from collection.
 *
 * @param array $inputs
 *
 * @return string
 */
if (! function_exists('generateListElement')) {
    function generateListElement(array $inputs, array $columns = ['name'])
    {
        $collection = collect($inputs);

        $list = '<ul>%s</ul>';

        $elements = $collection->map(function ($item) use ($columns) {
            if (count($columns) > 1) {
                $name = '';

                foreach ($columns as $key => $column) {
                    if ($key === 0) {
                        $name = $name . $item[$column] . ' - ';
                    } else {
                        $name = $name . $item[$column];
                    }
                };
            } else {
                $name = $item[Arr::first($columns)];
            }

            return '<li>' . $name . '</li>';
        })->all();

        return sprintf($list, implode('', $elements));
    }
}