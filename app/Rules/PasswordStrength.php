<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PasswordStrength implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // (?=.*\d)           => there is at least one digit
        // (?=.*[a-z])        => there is at least one lowercase character
        // (?=.*[A-Z])        => there is at least one uppercase character
        // (?=.*[#$@!%&*?])   => there is at least one special character
        // .{14,}              => length is 8 or more
        return preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[#$@!%&*?]).{8,}/', $value, $match);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must contain at least one digit, one lowercase character, one uppercase character and one special character.';
    }
}
