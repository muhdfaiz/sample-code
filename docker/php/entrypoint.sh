#!/bin/bash

# start the cronjob for Task Schefuler
service cron start

set -e

if [ ! -f /var/www/.env ]; then
    cp /var/www/.env.example .env
fi

chmod -R 0755 /var/www/githooks/

cp -rp /var/www/githooks /var/www/.git/hooks

composer install

php artisan key:generate
#
php artisan migrate

exec php-fpm

exec "$@"
