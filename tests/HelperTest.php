<?php

namespace Tests;

class HelperTest extends  TestCase
{
    public function testGenerateElements()
    {
        $roles = [
            ['name' => 'superadmin'],
            ['name' => 'manager'],
            ['name' => 'apiuser']
        ];

        $listElements = generateListElement($roles);

        $this->assertEquals('<ul><li>superadmin</li><li>manager</li><li>apiuser</li></ul>', $listElements);
    }
}
