<?php

namespace Tests;

use App\Rules\PasswordStrength;

class PasswordStrengthRuleTest extends TestCase
{
    public function testPasswordRuleShouldPass()
    {
        $rule = new PasswordStrength();

        $this->assertEquals(1, $rule->passes('password', 'TesT12@@@@@'));
    }

    public function testPasswordRuleMustContainOneSymbol()
    {
        $rule = new PasswordStrength();

        $this->assertEquals(0, $rule->passes('password', 'TesT12123123123'));
    }

    public function testPasswordRuleMustContainOneUppercase()
    {
        $rule = new PasswordStrength();

        $this->assertEquals(0, $rule->passes('password', 'test12123123123@@@'));
    }

    public function testPasswordRuleMustContainOneLowercase()
    {
        $rule = new PasswordStrength();

        $this->assertEquals(0, $rule->passes('password', 'TEST123213@@@'));
    }

    public function testPasswordRuleMustContainOneNumber()
    {
        $rule = new PasswordStrength();

        $this->assertEquals(0, $rule->passes('password', 'TEST#######@@@'));
    }
}
