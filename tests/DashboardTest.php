<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DashboardTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Testing showing the password reset request page.
     */
    public function testShowDashboardPage()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $this->actingAs($user)->get(route('dashboard.index'))
            ->assertSuccessful()
            ->assertSee('Dashboard')
            ->assertSee('Total User')
            ->assertSee('Total Customer');
    }
}
