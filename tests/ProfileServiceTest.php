<?php

namespace Tests;

use App\Models\User;
use App\Services\ProfileService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;

class ProfileServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testUpdateProfileWithoutPasswordShouldSuccess()
    {
        $user = User::create([
            'api_token' => 'adminuser',
            'username'  => 'admin',
            'email'  => 'admin@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ]);

        $profileService = new ProfileService();

        $inputs = [
            'username' => 'testuser',
            'email'  => 'testuser@test.com',
            'phone'  => '0121234567',
        ];

        $updatedUser = $profileService->updateProfile($user, $inputs);

        $this->assertSame($inputs['username'], $updatedUser->username);
        $this->assertSame($inputs['email'], $updatedUser->email);
        $this->assertSame($inputs['phone'], $updatedUser->phone);

        // Check password still same.
        $this->assertTrue(Hash::check('123456', $updatedUser->password));
        $this->assertSame($user->api_token, $user->api_token);
        $this->assertSame($user->is_active, $updatedUser->is_active);
    }

    public function testUpdateProfilePasswordShouldSuccess()
    {
        $user = User::create([
            'api_token' => 'adminuser',
            'username'  => 'admin',
            'email'  => 'admin@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ]);

        $profileService = new ProfileService();

        $inputs = [
            'username' => 'admin',
            'email'  => 'admin@example.com',
            'phone'  => '',
            'password'  => 'abcd1234',
        ];

        $updatedUser = $profileService->updateProfile($user, $inputs);

        $this->assertSame($inputs['username'], $updatedUser->username);
        $this->assertSame($inputs['email'], $updatedUser->email);
        $this->assertSame($inputs['phone'], $updatedUser->phone);

        // Check password still same.
        $this->assertTrue(Hash::check('abcd1234', $updatedUser->password));
        $this->assertSame($user->api_token, $user->api_token);
        $this->assertSame($user->is_active, $updatedUser->is_active);
    }
}
