<?php

/**
 * Tests the EncryptionMiddleware
 */

namespace Tests;

use App\Http\Middleware\EncryptionMiddleware;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mockery;

class EncryptionMiddlewareTest extends TestCase
{
    private $enabledConfigValue;

    private $testData = 'secret data we want to encrypt';

    public function setUp(): void
    {
        parent::setUp();

        // force encryption enabled in case it's switched off
        $this->enabledConfigValue = config('aws.kms.enable_encryption');
        config()->set('aws.kms.enable_encryption', true);
    }

    /** @test */
    public function testResponsesAreEncrypted()
    {
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', 'someString');

        $response = Mockery::Mock(Response::class)
                           ->shouldReceive('header')->with('Data-Encrypted', true)->once()
                           ->shouldReceive('setContent')
                           ->withArgs(function ($arg) {
                               // expect that the content is a JSON array
                            if (is_array(json_decode($arg, true))) {
                                return true;
                            }

                               return false;
                           })
                           ->withArgs(function ($arg) {
                               // expect that the response conforms to
                               // a structure
                               $arr = json_decode($arg, true);
                            if (isset($arr['cipherData'])
                                   && isset($arr['cipherKey'])
                               ) {
                                return true;
                            }

                               return false;
                           })
                           ->getMock();

        // add the data to be encrypted
        $response->original = $this->testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        // suppress the Risky test because it doesn't have assertion
        // warning.
        $this->assertEquals(1, 1);
    }

    /**
     * When encryption is disabled, the response should not be modified
     */
    public function testEncryptionDisabled()
    {
        config()->set('aws.kms.enable_encryption', false);

        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', 'someString');

        $response = Mockery::Mock(Response::class)
                           ->shouldNotReceive('setContent')
                           ->getMock();

        // add the data to be encrypted
        $response->original = $this->testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        // suppress the Risky test because it doesn't have assertion
        // warning.
        $this->assertEquals(1, 1);
    }

    /** @test */
    public function testRequireDataInArray()
    {
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', 'someString');

        $response = Mockery::Mock(Response::class)
                           ->shouldReceive('setContent')
                           ->withAnyArgs()
                           ->getMock();

        // this property is not provided:
        // $response->original =  $this->testData;

        $this->expectException('\Exception');

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     *Test decrypting some dynamic data
     */
    public function testDynamicDecryptionMatch()
    {
        $random = rand();

        $testData = 'This is a sentence which will have random number: ' . rand();

        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);
        $context = ['ctx' => $random];

        $response = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')
                           ->with('Data-Encrypted', true)->once()->getMock();
        $response->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        $encrypted = json_decode($response->content(), true);

        $decrypted = $middleware->decryptEnvelope($encrypted, $context);
        $this->assertEquals($testData, json_decode($decrypted, true));
    }

    /**
     *Test decrypting a large data array
     */
    public function testDecryptLargeArray()
    {
        $random = rand();
        $testData = json_decode(file_get_contents('./storage/tests/largeJsonArray.json'), true);

        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);
        $context = ['ctx' => $random];

        $response = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        $encrypted = json_decode($response->content(), true);

        $decrypted = $middleware->decryptEnvelope($encrypted, $context);

        $this->assertJsonStringEqualsJsonString(json_encode($testData), $decrypted);
    }


    /**
     *Test decrypting some dynamic data
     */
    public function testDecryptionWrongContextFail()
    {
        $random = rand();

        $testData = 'This is a sentence which will have random number: ' . rand();

        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);

        $response = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        $encrypted = json_decode($response->content(), true);
        $context = ['ctx' => $random + 1];

        $this->expectException('\Exception');
        $decrypted = $middleware->decryptEnvelope($encrypted, $context);
    }

    /**
     *Test decrypting some dynamic data
     */
    public function testDecryptionNoContextFail()
    {
        $random = rand();

        $testData = 'This is a sentence which will have random number: ' . rand();

        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);

        $response = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });

        $encrypted = json_decode($response->content(), true);
        $context = ['ctx' => null];

        $this->expectException('\Exception');
        $decrypted = $middleware->decryptEnvelope($encrypted, $context);
    }




    /**
     * If we send a request without the required context header
     * we will get an exception.
     */
    public function testRequireContextHeader()
    {
        $request = Request::create('/', 'GET');

        // don't include this header:
        // $request->headers->set('ctx', 'someString');

        $this->expectException('\Exception');

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () {
            return null;
        });
    }

    /**
     * Include the context but make it empty
     */
    public function testRequireContextHeaderNotEmpty()
    {
        $request = Request::create('/', 'GET');

        // don't include this header:
        $request->headers->set('ctx', null);

        $this->expectException('\Exception');

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () {
            return null;
        });
    }

    /**
     * The encryption key is cached
     */
    public function testDataKeyIsCached()
    {
        $random = rand();
        $testData = $this->testData;
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);

        $response1 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response1->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response1) {
            return $response1;
        });

        // check the cache
        $cached = \Cache::get(md5('CommsDataKey-' . $random));
        $this->assertNotEmpty($cached);

        // test the request again and see that the cipherKey is the same
        $response2 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response2->original = $testData;

        $middleware->handle($request, function () use ($response2) {
            return $response2;
        });

        $content1 = json_decode($response1->content(), true);
        $content2 = json_decode($response2->content(), true);
        $this->assertEquals($content1['cipherKey'], $content2['cipherKey']);
    }

    /**
     * Different contexts produce different data keys
     */
    public function testDifferentContextsYieldDifferentCipherKey()
    {
        $random = rand();
        $testData = $this->testData;
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);

        $response1 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response1->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response1) {
            return $response1;
        });

        // repeat the request
        unset($request);

        $random = rand();
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);
        $response2 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response2->original = $testData;

        $middleware->handle($request, function () use ($response2) {
            return $response2;
        });

        $content1 = json_decode($response1->content(), true);
        $content2 = json_decode($response2->content(), true);

        $this->assertNotEquals($content1['cipherKey'], $content2['cipherKey']);
    }

    /**
     * Even though the plain text message is identical, and the key is cached
     * the cipher text  produced by the encryption should be different
     */
    public function testIdenticalPlainTextYieldsDifferentCipherText()
    {
        $random = rand();
        $testData = $this->testData;
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', $random);

        $response1 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();
        $response1->original = $testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response1) {
            return $response1;
        });

        // repeat the request
        $response2 = Mockery::Mock(Response::class)->makePartial()->shouldReceive('header')->with(
            'Data-Encrypted',
            true
        )->once()->getMock();

        $response2->original = $testData;

        $middleware->handle($request, function () use ($response2) {
            return $response2;
        });

        $content1 = json_decode($response1->content(), true);
        $content2 = json_decode($response2->content(), true);

        $this->assertEquals($content1['cipherKey'], $content2['cipherKey']);
        $this->assertNotEquals($content1['cipherData'], $content2['cipherData']);
    }

    /**
     * Using a missing or wrong CMK alias will throw an exception
     */
    public function testInvalidCMKAlias()
    {
        config()->set('aws.kms.cmk_arn', 'alias/that_is_probably_not_going_to_be_real-' . rand());

        $this->expectException('\Exception');
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', 'someString');

        $response = Mockery::Mock(Response::class)->makePartial();
        $response->original = $this->testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });
    }

    /**
     * Using a missing or wrong CMK alias will throw an exception
     */
    public function testMissingCMKAlias()
    {
        config()->set('aws.kms.cmk_arn', null);

        $this->expectException('\Exception');
        $request = Request::create('/', 'GET');
        $request->headers->set('ctx', 'someString');

        $response = Mockery::Mock(Response::class)->makePartial();
        $response->original = $this->testData;

        $middleware = new EncryptionMiddleware;
        $middleware->handle($request, function () use ($response) {
            return $response;
        });
    }

    public function teardown(): void
    {
        // set back the env variable to the original setting
        config()->set('aws.kms.enable_encryption', $this->enabledConfigValue);
        Mockery::close();
    }
}
