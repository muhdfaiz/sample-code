<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Role;

class RoleTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'RolesTableSeeder']);
    }

    public function testOnlySuperAdminCanViewRoles()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $this->actingAs($user)->get(route('roles.index'))
            ->assertStatus(403);
    }

    public function testOnlySuperAdminCanViewCreateRole()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $this->actingAs($user)->get(route('roles.create'))
            ->assertStatus(403);
    }

    public function testOnlySuperAdminCanViewEditRole()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $role = Role::first();

        $this->actingAs($user)->get(route('roles.edit', $role))
            ->assertStatus(403);
    }

    public function testOnlySuperAdminCanViewDeleteRole()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $role = Role::first();

        $this->actingAs($user)->get(route('roles.destroy', $role))
            ->assertStatus(403);
    }

    public function testShowViewRolesPage()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $this->actingAs($user)->get(route('roles.index'))
            ->assertStatus(200)
            ->assertSee('Available Roles')
            ->assertSee('Create New Role');
    }

    public function testShowCreateRolePage()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $this->actingAs($user)->get(route('roles.create'))
            ->assertStatus(200)
            ->assertSee('Create New Role');
    }

    public function testCreateRoleShouldSuccess()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $inputs = [
            'name' => 'testrole'
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('roles.create'))
            ->postJson(route('roles.store'), $inputs)
            ->assertStatus(200)
            ->assertSee('Successfully created role ' . $inputs['name']);

        $this->assertDatabaseHas(config('permission.table_names.roles'), ['name' => $inputs['name']]);
    }

    public function testRequiredFieldsDuringCreateRole()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $inputs = [
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('roles.create'))
            ->postJson(route('roles.store'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'name' => 'The name field is required.'
            ]);
    }

    public function testUpdateRoleShouldSuccess()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username' => 'testuser',
            'email' => 'testuser@test.com',
            'password' => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $role = Role::first();

        $inputs = [
            'name' => 'testrole'
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('roles.edit', $role))
            ->putJson(route('roles.update', $role), $inputs)
            ->assertStatus(200)
            ->assertSee('Role has been updated.');

        $this->assertSame($inputs['name'], $role->refresh()->name);
    }

    public function testRequiredFieldsDuringUpdateRole()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $inputs = [
            'name' => ''
        ];

        $role = Role::first();

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('roles.edit', $role))
            ->putJson(route('roles.update', $role), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'name' => 'The name field is required.'
            ]);
    }

    public function testDeleteRoleShouldBeSuccess()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['superadmin']);

        $role = Role::first();

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('roles.index'))
            ->delete(route('roles.destroy', $role), $inputs)
            ->assertStatus(200);

        $this->assertDatabaseMissing(config('permission.table_names.roles'), [
            'name' => $role->name
        ]);
    }
}
