<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'RolesTableSeeder']);
    }

    /**
     * Testing showing the login page.
     */
    public function testShowLoginPage()
    {
        $this->get('/login')
            ->assertSuccessful()
            ->assertSee('Login')
            ->assertSee('Remember Me')
            ->assertSee('I forgot my password');
    }

    public function testPasswordAndEmailOrUsernameRequireDuringLogin()
    {
        $response = $this->postJson('/login');

        $response->assertJsonValidationErrors([
            'email_or_username' => 'The email or username field is required.',
            'password' => 'The password field is required.'
        ]);
        $response->assertStatus(422);
    }

    public function testLoginUsingInvalidUsernameShouldFailed()
    {
        $inputs = [
            'email_or_username' => 'test',
            'password' => '123456'
        ];

        $response = $this->postJson('/login', $inputs);

        $response->assertJsonValidationErrors([
            'email_or_username' => 'These credentials do not match our records.',
        ]);
        $response->assertStatus(422);
    }

    public function testLoginUsingInvalidPasswordShouldFailed()
    {
        $inputs = [
            'api_token' => 'invalidpassword',
            'username'  => 'invalidpassword',
            'email'  => 'invalidpassword@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole('superadmin');

        $inputs = [
            'email_or_username' => 'invalidpassword',
            'password' => 'abc123'
        ];

        $response = $this->postJson('/login', $inputs);

        $response->assertJsonValidationErrors([
            'email_or_username' => 'These credentials do not match our records.',
        ]);
        $response->assertStatus(422);
    }

    public function testLoginUsingUsernameShouldSuccess()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole('superadmin');

        $loginInputs = [
            'email_or_username' => 'testuser',
            'password' => '123456'
        ];

        $response = $this->postJson('/login', $loginInputs);

        $response->assertRedirect(route('dashboard.index'));
        $response->assertStatus(302);
    }

    public function testLoginUsingEmailShouldSuccess()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        User::create($inputs);

        $loginInputs = [
            'email_or_username' => 'testuser@test.com',
            'password' => '123456'
        ];

        $response = $this->postJson('/login', $loginInputs);

        $response->assertRedirect(route('dashboard.index'));
        $response->assertStatus(302);
    }

    public function testLoginUsingUserNotActiveShouldFailed()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 0,
        ];

        User::create($inputs);

        $loginInputs = [
            'email_or_username' => 'testuser@test.com',
            'password' => '123456'
        ];

        $response = $this->postJson('/login', $loginInputs);

        $response->assertJsonValidationErrors([
            'email_or_username' => 'These credentials do not match our records.',
        ]);
        $response->assertStatus(422);
    }

    public function testLoginUsingUserWithRoleApiUserShouldFailed()
    {
        $inputs = [
            'api_token' => 'apiuser',
            'username'  => 'apiuser',
            'email'  => 'apiuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole('apiuser');

        $loginInputs = [
            'email_or_username' => $inputs['email'],
            'password' => '123456'
        ];

        $response = $this->postJson('/login', $loginInputs);

        $response->assertJsonValidationErrors([
            'email_or_username' => 'These credentials do not match our records.',
        ]);
        $response->assertStatus(422);
    }
}
