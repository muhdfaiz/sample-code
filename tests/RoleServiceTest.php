<?php

namespace Tests;

use App\Services\RoleService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Arr;
use App\Models\Role;

class RoleServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'RolesTableSeeder']);
    }

    public function testGetAllRolesShouldSuccess()
    {
        $roleService = new RoleService();

        $data = $roleService->getAllRole()->getData(true);

        $this->assertCount(3, $data['data']);

        $expectedRoles = implode(',', Role::pluck('name')->toArray());
        $actualRoles = implode(',', Arr::pluck($data['data'], 'name'));

        $this->assertSame($expectedRoles, $actualRoles);
    }

    public function testCreateNewRoleShouldSuccess()
    {
        $roleService = new RoleService();

        $inputs = [
          'name' => 'testrole'
        ];

        $role = $roleService->createNewRole($inputs);

        $this->assertSame($inputs['name'], $role->name);
        $this->assertSame('web', $role->guard_name);
    }

    public function testUpdateRoleShouldSuccess()
    {
        $roleService = new RoleService();

        $inputs = [
            'name' => 'testrole2'
        ];

        $role = Role::first();

        $result = $roleService->updateRole($role, $inputs);

        $this->assertTrue($result);
        $this->assertSame($inputs['name'], $role->refresh()->name);
    }

    public function testDeleteRoleShouldSuccess()
    {
        $roleService = new RoleService();

        $role = Role::first();

        $result = $roleService->deleteRole($role);

        $this->assertTrue($result);
        $this->assertDatabaseMissing(config('permission.table_names.roles'), ['name' => $role->name]);
    }
}