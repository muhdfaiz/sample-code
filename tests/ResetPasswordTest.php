<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ResetPasswordTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Testing showing the reset password page.
     */
    public function testShowPasswordResetPage()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $this->get(route('password.reset', [
                'token' => $token,
            ]))->assertSuccessful()
            ->assertSee('Reset Password')
            ->assertSee('New Password')
            ->assertSee('New Password Confirmation');
    }

    /**
     * Testing submitting the password reset page with an invalid
     * email address.
     */
    public function testSubmitPasswordResetInvalidEmail()
    {
        $originalPassword = '123456';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TesT1234@@';

        $this->followingRedirects()
            ->from(route(
                'password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'test@gmail.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])->assertSuccessful()
            ->assertSee('We can&#039;t find a user with that e-mail address.');

        $user->refresh();

        $this->assertFalse(Hash::check($newPassword, $user->password));
        $this->assertTrue(Hash::check($originalPassword, $user->password));
    }

    /**
     * Testing submitting the password reset page with a password
     * that is not long enough.
     */
    public function testSubmitPasswordResetPasswordTooShort()
    {
        $originalPassword = 'TesT1234@@';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'Sa1234@@';

        $this
            ->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertSee(__('validation.min.string', [
                'attribute' => 'password',
                'min' => 14,
            ]));

        $user->refresh();

        $this->assertTrue(Hash::check($originalPassword, $user->password));
        $this->assertFalse(Hash::check($newPassword, $user->password));
    }

    public function testRequiredFields()
    {
        $originalPassword = '123456';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $response = $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
            ]);

        $response->assertJsonValidationErrors([
            'email' => 'The email field is required.',
            'password' => 'The password field is required.',
        ]);
    }

    public function testSubmitPasswordResetShouldSuccess()
    {
        $originalPassword = '123456';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TesT@@@111';

        $this
            ->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertSuccessful()
            ->assertSee(__('passwords.reset'));

        $user->refresh();

        $this->assertFalse(Hash::check($originalPassword, $user->password));
        $this->assertTrue(Hash::check($newPassword, $user->password));
    }

    public function testPasswordMinimumLengthIs14()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TesT@1';

        $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must be at least 14 characters.',
            ]);
    }

    public function testPasswordMustContainOneSymbol()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TesT11111';

        $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneDigit()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TesT@@@@#####';

        $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneUppercase()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'test111@@';

        $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneLowercase()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $token = Password::broker()->createToken($user);

        $newPassword = 'TEST1234@@';

        $this->followingRedirects()
            ->from(route('password.reset', [
                'token' => $token,
            ]))->postJson(route('password.update'), [
                'token' => $token,
                'email' => 'testuser@test.com',
                'password' => $newPassword,
                'password_confirmation' => $newPassword,
            ])
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }
}
