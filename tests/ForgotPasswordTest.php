<?php

namespace Tests;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Notification;

class ForgotPasswordTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Testing showing the password reset request page.
     */
    public function testShowPasswordResetRequestPage()
    {
        $this->get(route('password.request'))
            ->assertSuccessful()
            ->assertSee('Forgot Password')
            ->assertSee('Enter your email and instructions will be sent to you!')
            ->assertSee('Send Password Reset Link');
    }

    public function testEmailFieldRequireDuringPasswordReset()
    {
        $response = $this->postJson(route('password.email'));

        $response->assertJsonValidationErrors([
            'email' => 'The email field is required.',
        ]);

        $response->assertStatus(422);
    }

    public function testResetPasswordUsingInvalidEmailShouldFailed()
    {
        $input = [
            'email' => 'test@gmail.com',
        ];

        $response = $this->followingRedirects()->withSession(['_previous.url' => route('password.request')])
            ->postJson(route('password.email'), $input);

        $response->assertSuccessful()
            ->assertSee(e(__('passwords.user')));
    }

    public function testSubmitPasswordResetRequestShouldSuccess()
    {
        Notification::fake();

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $this->followingRedirects()
            ->from(route('password.request'))
            ->post(route('password.email'), [
                'email' => $user->email,
            ])->assertSuccessful()
            ->assertSee(e(__('passwords.sent')));

        Notification::assertSentTo($user, ResetPassword::class);
    }
}