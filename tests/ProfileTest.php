<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;

class ProfileTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed', ['--class' => 'RolesTableSeeder']);
        $this->artisan('db:seed', ['--class' => 'UsersTableSeeder']);
    }

    public function testShowUpdateProfilePage()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $this->actingAs($user)->get(route('profiles.edit'))
            ->assertStatus(200)
            ->assertSee('Edit User')
            ->assertSee('Save');
    }

    public function testRequiredFieldsDuringUpdateProfile()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => '',
            'email' => '',
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'username' => 'The username field is required.',
                'email' => 'The email field is required.'
            ]);
    }

    public function testPasswordMinimumLengthIs14()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => $inputs['username'],
            'email' => $inputs['email'],
            'password' => 'Sales@@12',
            'password_confirmation' => 'Sales@@12',
            'phone' => ''
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must be at least 14 characters.',
            ]);
    }

    public function testPasswordMustContainOneSymbol()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => $inputs['username'],
            'email' => $inputs['email'],
            'password' => 'TesT123456',
            'password_confirmation' => 'TesT123456',
            'phone' => ''
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneDigit()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => $inputs['username'],
            'email' => $inputs['email'],
            'password' => 'TesT@@@@@@@@@@@@@@@',
            'password_confirmation' => 'TesT@@@@@@@@@@@@@@@',
            'phone' => ''
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneUppercase()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => $inputs['username'],
            'email' => $inputs['email'],
            'password' => 'test2019@@@@@',
            'password_confirmation' => 'test2019@@@@@',
            'phone' => ''
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testPasswordMustContainOneLowercase()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => $inputs['username'],
            'email' => $inputs['email'],
            'password' => 'TEST2019@@@@@',
            'password_confirmation' => 'TEST2019@@@@@',
            'phone' => ''
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password must contain at least one digit, one lowercase character, one uppercase character and one special character.',
            ]);
    }

    public function testEmailAndUsernameShouldBeUnique()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => 'admin',
            'email' => 'admin@example.com',
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'username' => 'The username has already been taken.',
                'email' => 'The email has already been taken.',
            ]);
    }

    public function testPasswordAndPasswordConfirmationMustMatch()
    {
        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => 'testuser',
            'email' => 'testuser@example.com',
            'password' => 'abc123',
            'password_confirmation' => 'abc1231',
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'password' => 'The password confirmation does not match.',
            ]);
    }

    public function testUpdateProfileWithPasswordShouldSuccess()
    {
        $originalPassword = 'TesT@@1234567890';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $newPassword = 'TesT@@1234567890';

        $inputs = [
            'username' => 'testuserupdate',
            'email' => 'testuserupdate@example.com',
            'password' => $newPassword,
            'password_confirmation' => $newPassword,
            'phone' => '012123456789',
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(200)
            ->assertSee('Your profile has been updated.');

        $updatedUser = $user->refresh();

        $this->assertSame($inputs['username'], $updatedUser->username);
        $this->assertSame($inputs['email'], $updatedUser->email);
        $this->assertSame($inputs['phone'], $updatedUser->phone);
        $this->assertTrue(Hash::check($newPassword, $updatedUser->password));
        $this->assertFalse(Hash::check($originalPassword, $updatedUser->password));
    }

    public function testUpdateProfileWithoutPasswordShouldSuccess()
    {
        $originalPassword = 'TesT@@1234567890';

        $inputs = [
            'api_token' => 'testuser',
            'username'  => 'testuser',
            'email'  => 'testuser@test.com',
            'password'  => bcrypt($originalPassword),
            'is_active' => 1,
        ];

        $user = User::create($inputs);

        $user->assignRole(['manager']);

        $inputs = [
            'username' => 'testuser',
            'email' => 'testuser@test.com',
            'phone' => '012123456789',
        ];

        $this->actingAs($user)
            ->followingRedirects()
            ->from(route('profiles.edit'))
            ->putJson(route('profiles.update'), $inputs)
            ->assertStatus(200)
            ->assertSee('Your profile has been updated.');

        $updatedUser = $user->refresh();

        $this->assertSame($inputs['username'], $updatedUser->username);
        $this->assertSame($inputs['email'], $updatedUser->email);
        $this->assertSame($inputs['phone'], $updatedUser->phone);
        $this->assertTrue(Hash::check($originalPassword, $updatedUser->password));
    }
}
