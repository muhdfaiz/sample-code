# Set the base image for subsequent instructions
FROM mashhadi/sc-php-prod:latest

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY .env.example .env

RUN composer install --no-dev --no-interaction -o

## Install Laravel Envoy
#RUN composer global require "laravel/envoy=~1.0"