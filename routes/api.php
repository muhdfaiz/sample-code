<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api', 'cors', 'encryption']], function ($router) {
    // customers
    $router->group(['prefix' => 'customers', 'namespace' => 'Api'], function ($router) {
        $router->get('index', 'CustomersController@index');
    });
});
