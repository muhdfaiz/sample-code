<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Generate authentication routes.
Auth::routes(['register' => false, 'verify' => false]);

Route::group(['middleware' => 'auth', 'namespace' => 'Dashboard'], function() {
    // Route related to dashboard.
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');

    // Routes related to profile.
    Route::get('/profile', 'ProfileController@edit')->name('profiles.edit');
    Route::put('/profile', 'ProfileController@update')->name('profiles.update');

    Route::group(['middleware' => ['role:superadmin']], function () {
        // Routes related to role.
        Route::get('roles/get', 'RoleController@get')->name('roles.get');
        Route::resource('roles', 'RoleController');

        // Routes related to user.
        Route::patch('users/{user}/api_key', 'UserController@updateApiKey')->name('users.update.api_key');
        Route::get('users/get', 'UserController@get')->name('users.get');
        Route::resource('users', 'UserController');
    });
});



