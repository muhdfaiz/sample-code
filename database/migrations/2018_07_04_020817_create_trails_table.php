<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trails', function (Blueprint $table) {
            $table->engine = 'Aria';
            $table->bigIncrements('id');
            $table->string('eventversion')->nullable();
            $table->json('useridentity')->nullable();
            $table->dateTime('eventtime')->nullable();
            $table->string('eventsource')->nullable();
            $table->string('eventname')->nullable();
            $table->string('awsregion')->nullable();
            $table->string('sourceipaddress')->nullable();
            $table->string('useragent')->nullable();
            $table->string('errorcode')->nullable();
            $table->string('errormessage')->nullable();
            $table->json('resources')->nullable();
            $table->json('requestparameters')->nullable();
            $table->json('responseelements')->nullable();
            $table->string('additionaleventdata')->nullable();
            $table->string('requestid')->nullable();
            $table->string('eventid')->nullable();
            $table->string('eventtype')->nullable();
            $table->string('apiversion')->nullable();
            $table->boolean('readonly')->nullable();
            $table->string('recipientaccountid')->nullable();
            $table->string('serviceeventdetails')->nullable();
            $table->string('sharedeventid')->nullable();
            $table->string('vpcendpointid')->nullable();
            $table->timestamps();

            $table->index('eventtime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trails');
    }
}
