<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('shortName');
            $table->string('companyLogo');
            $table->integer('countryId')->nullable()->unsigned();
            $table->integer('timezoneId')->nullable()->unsigned();
            $table->integer('activeProjectsLimit');
            $table->integer('activeSalespersonLimit');
            $table->integer('activeLeadLimit');
            $table->integer('monthlyProcessedLeadLimit');
            $table->string('managerPortalSessionTimeout');
            $table->string('coolDownDuration');
            $table->integer('routingPoolId')->nullable()->unsigned();
            $table->timestamps();

            $table->foreign('countryId')
                ->references('id')->on('countries')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('timezoneId')
                ->references('id')->on('timezones')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('routingPoolId')
                ->references('id')->on('routing_pools')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropForeign(['countryId']);
            $table->dropForeign(['timezoneId']);
            $table->dropForeign(['routingPoolId']);
        });
        Schema::dropIfExists('accounts');
    }
}
