<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
//            $table->json('tags')->nullable();
//            $table->json('domainPrefixes')->nullable();
            $table->string('alphaTwoCode', 3)->nullable();
            $table->string('alphaThreeCode', 4)->nullable();
            $table->json('callingCodes')->nullable();
//            $table->json('capital')->nullable();
//            $table->json('altSpellings')->nullable();
//            $table->json('region')->nullable();
//            $table->json('subRegion')->nullable();
//            $table->integer('population')->nullable();
//            $table->float('lat')->nullable();
//            $table->float('long')->nullable();
//            $table->json('demonym')->nullable();
//            $table->float('area', 15, 5)->nullable();
//            $table->float('gini')->nullable();
            $table->json('timezones')->nullable();
//            $table->json('borders')->nullable();
//            $table->string('nativeName', 60)->nullable();
//            $table->smallInteger('numericCode')->nullable();
            $table->json('currencies')->nullable();
//            $table->json('languages')->nullable();
//            $table->json('translations')->nullable();
//            $table->string('flag')->nullable();
//            $table->json('regionalBlocs')->nullable();
            $table->string('cioc', 4)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
