<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('TimezonesTableSeeder');
        $this->call('CountriesTableSeeder');
        $this->call('RoutingPoolsTableSeeder');
        $this->call(RolesTableSeeder::class);
        $this->call('UsersTableSeeder');
        $this->call('CustomersTableSeeder');
    }
}
