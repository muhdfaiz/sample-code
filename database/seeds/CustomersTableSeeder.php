<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CustomersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        // number of rows to seed:
        $end = 150;

        DB::table('customers')->delete();

        $faker = Faker::create();
        foreach (range(1, $end) as $index) {

            // randomly put notes in 20% of rows
            $notes = null;
            if (rand(0, 100) < 20) {
                $notes = $faker->text();
            };

            DB::table('customers')->insert([
                'name'       => $faker->name,
                'ref_id'     => $faker->uuid,
                'email'      => $faker->email,
                'phone'      => $faker->phoneNumber,
                'address'    => $faker->address,
                'notes'      => $notes,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
