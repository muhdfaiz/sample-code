<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: muhammadshoaib
 * Date: 04/05/2018
 * Time: 3:12 PM
 */
class TimezonesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('timezones')->delete();
        $timezones = json_decode(file_get_contents('timezones.json'));
        $data = [];
        foreach ($timezones as $timezone) {

            $data[] = [
                'value' => $timezone->value,
                'abbr' => $timezone->abbr,
                'offset' => $timezone->offset,
                'isdst' => $timezone->isdst,
                'text' => $timezone->text,
                'utc' => json_encode($timezone->utc),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }
        DB::table('timezones')->insert($data);
    }
}