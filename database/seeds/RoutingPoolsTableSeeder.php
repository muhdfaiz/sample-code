<?php
use Illuminate\Database\Seeder;
/**
 * Created by PhpStorm.
 * User: muhammadshoaib
 * Date: 04/05/2018
 * Time: 6:47 PM
 */

class RoutingPoolsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('routing_pools')->delete();

        DB::table('routing_pools')->insert([
            ['name'=>'Worker (Routing 1)','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
            ['name'=>'Worker (Routing 2)','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
            ['name'=>'Worker (Routing 3)','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
            ['name'=>'Worker (Routing 4)','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
            ['name'=>'Worker (Routing 5)','created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')],
        ]);
    }
}