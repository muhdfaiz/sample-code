<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        $user = User::create([
            'api_token' => 'adminuser',
            'username'  => 'admin',
            'email'  => 'admin@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ]);

        $user->assignRole('superadmin');

        $user = User::create([
            'api_token' => 'apiuser',
            'username'  => 'apiuser',
            'email'  => 'apiuser@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 1,
        ]);

        $user->assignRole('apiuser');

        $user = User::create([
            'api_token' => 'disableduser',
            'username'  => 'elvis',
            'email'  => 'elvis@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 0,
        ]);

        $user->assignRole('apiuser');

        $user = User::create([
            'api_token' => 'disabledadminuser',
            'username'  => 'hawking',
            'email'  => 'hawking@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 0,
        ]);

        $user->assignRole('superadmin');

        $user->assignRole('apiuser');

        $user = User::create([
            'api_token' => 'manager',
            'username'  => 'manager',
            'email'  => 'manager@example.com',
            'password'  => bcrypt('123456'),
            'is_active' => 0,
        ]);

        $user->assignRole('manager');
    }
}
