<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: muhammadshoaib
 * Date: 03/05/2018
 * Time: 5:33 PM
 */

class CountriesTableSeeder extends Seeder{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->delete();

        $countries = json_decode(file_get_contents('countries.json'));
        $data = [];
        foreach ($countries as $country){

            $data[]=[
                'name'             => $country->name,
                'alphaTwoCode'   => $country->alpha2Code,
                'alphaThreeCode' => $country->alpha3Code,
                'callingCodes'    => json_encode($country->callingCodes),
                'timezones'        => json_encode($country->timezones),
                'currencies'       => json_encode($country->currencies),
                'cioc'             => $country->cioc,
                'created_at'       => date('Y-m-d H:i:s'),
                'updated_at'       => date('Y-m-d H:i:s'),
            ];
        }

        DB::table('countries')->insert($data);


    }
}