<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Illuminate\Support\Facades\DB;

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username'  => $faker->name,
        'email'     => $faker->email,
        'api_token' => str_random(32),
        'password'  => $faker->password,
        'phone'     => $faker->phoneNumber,
        'is_active' => $faker->boolean,
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'username'  => $faker->name,
        'email'     => $faker->email,
        'api_token' => str_random(32),
        'password'  => $faker->password,
        'phone'     => $faker->phoneNumber,
        'is_active' => 1,
    ];
});

$factory->define(App\Models\Account::class, function (Faker\Generator $faker) {
    return [
        'name'                        => $faker->userName,
        'shortName'                   => $faker->name,
        'companyLogo'                 => $faker->userName . ".png",
        'timezoneId'                  => DB::table('timezones')->first()->id,
        'countryId'                   => DB::table('countries')->first()->id,
        'activeProjectsLimit'         => '50',
        'activeSalespersonLimit'      => '50',
        'activeLeadLimit'             => '50',
        'monthlyProcessedLeadLimit'   => '50',
        'managerPortalSessionTimeout' => '30',
        'coolDownDuration'            => '2',
        'routingPoolId'               => DB::table('routing_pools')->first()->id,
    ];
});
