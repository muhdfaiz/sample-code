<?php
return [
    'credentials' => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
    ],
    'region'      => env('AWS_REGION', 'ap-southeast-1'),
    'version'     => 'latest',

    // You can override settings for specific services
    'kms'         => [
        'enable_encryption' => env('AWS_KMS_ENABLE_ENCRYPTION', true),
        'cmk_arn' => env('AWS_KMS_CMK_ARN'),
    ],
];
